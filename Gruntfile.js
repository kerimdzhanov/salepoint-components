/**
 * Tennisi salepoint components' Gruntfile.
 */

module.exports = function (grunt) {
  'use strict';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    clean: {
      dist: 'dist'
    },

    less: {
      compileCore: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'bootstrap.css.map',
          sourceMapFilename: 'dist/css/bootstrap.css.map'
        },
        src: 'less/bootstrap.less',
        dest: 'dist/css/bootstrap.css'
      },
      compileTheme: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'salepoint-theme.css.map',
          sourceMapFilename: 'dist/css/salepoint-theme.css.map'
        },
        src: 'less/salepoint-theme.less',
        dest: 'dist/css/salepoint-theme.css'
      },
      compileDashboard: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'dashboard.css.map',
          sourceMapFilename: 'dist/css/dashboard.css.map'
        },
        src: 'less/dashboard.less',
        dest: 'dist/css/dashboard.css'
      }
    },

    autoprefixer: {
      options: {
        browsers: [
          "Chrome >= 20",
          "Firefox >= 24",
          "Explorer >= 8",
          "Safari >= 6"
        ]
      },
      core: {
        options: {
          map: true
        },
        src: 'dist/css/bootstrap.css'
      },
      theme: {
        options: {
          map: true
        },
        src: 'dist/css/salepoint-theme.css'
      },
      dashboard: {
        options: {
          map: true
        },
        src: 'dist/css/dashboard.css'
      }
    },

    csslint: {
      options: {
        csslintrc: 'less/.csslintrc'
      },
      dist: [
        'dist/css/bootstrap.css',
        'dist/css/salepoint-theme.css',
        'dist/css/dashboard.css'
      ]
    },

    cssmin: {
      options: {
        compatibility: 'ie8',
        keepSpecialComments: '*',
        sourceMap: true,
        sourceMapInlineSources: true,
        advanced: false
      },
      minifyCore: {
        src: 'dist/css/bootstrap.css',
        dest: 'dist/css/bootstrap.min.css'
      },
      minifyTheme: {
        src: 'dist/css/salepoint-theme.css',
        dest: 'dist/css/salepoint-theme.min.css'
      },
      minifyDashboard: {
        src: 'dist/css/dashboard.css',
        dest: 'dist/css/dashboard.min.css'
      }
    },

    csscomb: {
      options: {
        config: 'less/.csscomb.json'
      },
      dist: {
        expand: true,
        cwd: 'dist/css/',
        src: ['*.css', '!*.min.css'],
        dest: 'dist/css/'
      }
    },

    copy: {
      fonts: {
        expand: true,
        cwd: 'node_modules/bootstrap',
        src: 'fonts/**',
        dest: 'dist/'
      }
    }
  });


  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, { scope: 'devDependencies' });
  require('time-grunt')(grunt);

  // CSS distribution task.
  grunt.registerTask('less-compile', [
    'less:compileCore',
    'less:compileTheme',
    'less:compileDashboard'
  ]);

  grunt.registerTask('build-css', [
    'less-compile',
    'autoprefixer:core',
    'autoprefixer:theme',
    'autoprefixer:dashboard',
    'csscomb:dist',
    'cssmin:minifyCore',
    'cssmin:minifyTheme',
    'cssmin:minifyDashboard'
  ]);

  // Full distribution task.
  grunt.registerTask('build', [
    'clean:dist',
    'build-css',
    'copy:fonts'
  ]);

  // Default task.
  grunt.registerTask('default', ['build']);
};
